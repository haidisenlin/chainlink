import sqlite3
from typing import List, Tuple

import chainlink_api.database_queries as sql


# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------
class ChainlinkDatabase(object):

    def __init__(self, db_file_path: str) -> None:
        super().__init__()
        self._db_file_path : str = db_file_path

    @property
    def db_file_path(self):
        return self._db_file_path

    def get_cursor(self):
        con = sqlite3.connect(self.db_file_path)
        cur = con.cursor()
        return cur

    def close_cursor(self, cursor):
        cursor.close()
        cursor.connection.close()

    def execute_query(self, query: str, values: tuple = None, commit: bool = False):
        cur = self.get_cursor()
        cur.execute(query, values if values is not None else tuple())
        if commit:
            cur.connection.commit()
        results = cur.fetchall()
        self.close_cursor(cur)
        return results


# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------
class Bookmark(object):
        
    def __init__(self, chainlink_db: ChainlinkDatabase, bookmark_id: int, bookmark_url: str, bookmark_display_text: str, bookmark_description: str) -> None:
        super().__init__()
        self._db_handler = chainlink_db
        self._bookmark_id = bookmark_id
        self._bookmark_url = bookmark_url
        self._bookmark_display_text = bookmark_display_text
        self._bookmark_description = bookmark_description

    @classmethod
    def init_from_row_tuple(cls, chainlink_db: ChainlinkDatabase, row_tuple: Tuple):
        return Bookmark(
            chainlink_db=chainlink_db,
            bookmark_id=row_tuple[0],
            bookmark_url=row_tuple[1],
            bookmark_display_text=row_tuple[2],
            bookmark_description=row_tuple[3]
        )

    @property
    def bookmark_id(self):
        return self._bookmark_id

    @property
    def bookmark_url(self):
        return self._bookmark_url;

    @property
    def bookmark_display_text(self):
        return self._bookmark_display_text;

    @property
    def bookmark_description(self):
        return self._bookmark_description;

    def update_display_text(self, bookmark_display_text: str):
        self._db_handler.execute_query(
            query=sql.queries.update_display_text_for_bookmark_with_id, 
            values=(bookmark_display_text, self.bookmark_id), 
            commit=True
        )

    def update_description(self, bookmark_description: str):
        self._db_handler.execute_query(
            query=sql.queries.update_description_for_bookmark_with_id, 
            values=(bookmark_description, self.bookmark_id), 
            commit=True
        )


# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------
class Tag(object):

    def __init__(self, chainlink_db: ChainlinkDatabase, tag_id: int, tag_name: str) -> None:
        super().__init__()
        self._db_handler = chainlink_db
        self._tag_id = tag_id
        self._tag_name = tag_name

    @classmethod
    def init_from_row_tuple(cls, chainlink_db: ChainlinkDatabase, row_tuple: Tuple):
        return Tag(
            chainlink_db=chainlink_db,
            tag_id=row_tuple[0],
            tag_name=row_tuple[1],
        )

    @property
    def tag_id(self):
        return self._tag_id

    @property
    def tag_name(self):
        return self._tag_name

    def update_tag_name(self, tag_name: str):
        self._db_handler.execute_query(
            query=sql.queries.update_tag_name_for_tag_with_id, 
            values=(tag_name, self._tag_id),
            commit=True
        )


# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------
class Chainlink(object):
    
    def __init__(self, db_file_path: str) -> None:
        super().__init__()
        self._db_handler = ChainlinkDatabase(db_file_path=db_file_path);
        self._init_db()

    def _init_db(self):
        self._db_handler.execute_query(sql.queries.create_bookmarks_table, commit=True)
        self._db_handler.execute_query(sql.queries.create_tags_table, commit=True)
        self._db_handler.execute_query(sql.queries.create_bookmark_tags_table, commit=True)

    @property
    def db_file_path(self):
        return self._db_handler.db_file_path
    
    def create_bookmark(self, bookmark_url: str, bookmark_name: str, bookmark_desc = "") -> None:
        self._db_handler.execute_query(
            query=sql.queries.insert_bookmark, 
            values=(None,bookmark_url,bookmark_name,bookmark_desc), 
            commit=True
        )

    def get_bookmark(self, bookmark_id: int) -> List[Bookmark]:
        _result = self._db_handler.execute_query(
            query=sql.queries.get_bookmark_with_id, 
            values=(bookmark_id,)
        )
        return [Bookmark.init_from_row_tuple(self._db_handler, row) for row in _result]

    def delete_bookmark(self, bookmark_id: int):
        self._db_handler.execute_query(
            query=sql.queries.delete_bookmark_with_id, 
            values=(bookmark_id,), 
            commit=True
        )

    def search_bookmarks_for_text(self, query_text: str) -> List[Bookmark]:
        _result = self._db_handler.execute_query(
            query=sql.queries.find_bookmark_containing_string, 
            values=tuple([f"%{query_text}%"]*3)
        )
        return [Bookmark.init_from_row_tuple(self._db_handler, row) for row in _result]

    def create_tag(self, tag_name: str):
        self._db_handler.execute_query(
            query=sql.queries.insert_tag, 
            values=(None,tag_name), 
            commit=True
        )

    def get_tag(self, tag_id: int):
        _results = self._db_handler.execute_query(
            query=sql.queries.get_tag_with_id, 
            values=(tag_id,)
        )
        return [ Tag.init_from_row_tuple(self._db_handler, row) for row in _results ]

    def search_tags_for_text(self, query_text: str):
        _results = self._db_handler.execute_query(
            query=sql.queries.find_tag_containing_string, 
            values=(f"%{query_text}%",)
        )
        return [ Tag.init_from_row_tuple(self._db_handler, row) for row in _results ]

    def delete_tag(self, tag_id: int):
        self._db_handler.execute_query(
            query=sql.queries.delete_tag_with_id, 
            values=(tag_id,), 
            commit=True
        )
        

    # ============================================================================
    class Builder(object):

        def set_database_file_path(self, db_file_path: str):
            self.db_file_path = db_file_path
            return self

        def build(self):
            return Chainlink(
                db_file_path=self.db_file_path
            )
