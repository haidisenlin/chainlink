# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------
class _Queries():

    @property
    def create_bookmarks_table(self):
        return """
        CREATE TABLE IF NOT EXISTS bookmarks (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            url TEXT NOT NULL UNIQUE,
            display_text TEXT NOT NULL,
            description TEXT
        );
        """

    @property
    def create_tags_table(self):
        return """
        CREATE TABLE IF NOT EXISTS tags (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL UNIQUE
        );
        """

    @property
    def create_bookmark_tags_table(self):
        return """
        CREATE TABLE IF NOT EXISTS bookmark_tags (
            bookmark_id INTEGER,
            tag_id INTEGER,
            CONSTRAINT BKMK_TAG PRIMARY KEY (bookmark_id, tag_id)
        );
        """

    @property
    def insert_bookmark(self):
        return """
        INSERT INTO bookmarks VALUES (?, ?, ?, ?);
        """
    
    @property
    def get_bookmark_with_id(self):
        return """
        SELECT * FROM bookmarks WHERE id = ?;
        """

    @property
    def find_bookmark_containing_string(self):
        return """
        SELECT * FROM bookmarks WHERE url LIKE ? OR display_text LIKE ? OR description LIKE ?;
        """

    @property
    def delete_bookmark_with_id(self):
        return """
        DELETE FROM bookmarks WHERE id = ?;
        """

    @property
    def insert_tag(self):
        return """
        INSERT INTO tags VALUES (?, ?);
        """

    @property
    def get_tag_with_id(self):
        return """
        SELECT * FROM tags WHERE id = ?;
        """

    @property
    def find_tag_containing_string(self):
        return """
        SELECT * FROM tags WHERE name LIKE ?;
        """

    @property
    def delete_tag_with_id(self):
        return """
        DELETE FROM tags WHERE id = ?;
        """


    @property
    def update_display_text_for_bookmark_with_id(self):
        return """
        UPDATE bookmarks SET display_text = ? WHERE id = ?;
        """
    
    @property
    def update_description_for_bookmark_with_id(self):
        return """
        UPDATE bookmarks SET description = ? WHERE id = ?;
        """

    @property
    def update_tag_name_for_tag_with_id(self):
        return """
        UPDATE tags SET name = ? WHERE id = ?;
        """


# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------
queries = _Queries()