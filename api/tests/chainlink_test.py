import os
import chainlink_api.chainlink_api as api

DB_PATH = '/tmp/test.db'

def _remove_file(file_path):
    if os.path.exists(file_path):
        os.remove(file_path)

def _before_test():
    _remove_file(DB_PATH)

def _after_test():
    _remove_file(DB_PATH)


def test_builder_creates_new_chainlink_instance():
    _before_test()

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    assert isinstance(chainlink, api.Chainlink)
    assert chainlink.db_file_path == DB_PATH

    _after_test()

# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------

def test_create_and_get_bookmark():
    _before_test()

    url = "https://www.duckduckgo.com"
    name = "DuckDuckGo"
    desc = ""

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_bookmark(bookmark_url=url, bookmark_name=name, bookmark_desc=desc)
    
    results = chainlink.get_bookmark(1)
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_id == 1
    assert result_bookmark.bookmark_url == url
    assert result_bookmark.bookmark_display_text == name
    assert result_bookmark.bookmark_description == desc

    _after_test()


def test_update_bookmark_display_text():
    _before_test()

    url = "https://www.duckduckgo.com"
    first_name = "DuckDuckGo"
    desc = ""

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_bookmark(bookmark_url=url, bookmark_name=first_name, bookmark_desc=desc)
    
    results = chainlink.get_bookmark(1)
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_display_text == first_name

    second_name="DDG - Private Search Engine"
    result_bookmark.update_display_text(second_name)

    results = chainlink.get_bookmark(1)
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_display_text == second_name

    _after_test()

def test_update_bookmark_description():
    _before_test()

    url = "https://www.duckduckgo.com"
    name = "DuckDuckGo"
    first_desc = ""

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_bookmark(bookmark_url=url, bookmark_name=name, bookmark_desc=first_desc)
    
    results = chainlink.get_bookmark(1)
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_description == first_desc

    second_desc="Protecting the world, one search result at a time."
    result_bookmark.update_description(second_desc)

    results = chainlink.get_bookmark(1)
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_description == second_desc

    _after_test()


def test_delete_bookmark():
    _before_test()

    url = "https://www.duckduckgo.com"
    name = "DuckDuckGo"
    desc = ""

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_bookmark(bookmark_url=url, bookmark_name=name, bookmark_desc=desc)
    
    results = chainlink.get_bookmark(1)
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_url == url

    chainlink.delete_bookmark(1)
    results = chainlink.get_bookmark(1)
    assert len(results) == 0

    _after_test()


def test_bookmark_search():
    _before_test()

    url = "https://www.duckduckgo.com"
    name = "DuckDuckGo"
    desc = "belcher"

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_bookmark(bookmark_url=url, bookmark_name=name, bookmark_desc=desc)
    
    results = chainlink.get_bookmark(1)
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_url == url

    results = chainlink.search_bookmarks_for_text('belcher')
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_url == url

    results = chainlink.search_bookmarks_for_text('Duck')
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_url == url

    results = chainlink.search_bookmarks_for_text('.com')
    assert len(results) == 1
    result_bookmark = results[0]
    assert result_bookmark.bookmark_url == url

    results = chainlink.search_bookmarks_for_text('hugo')
    assert len(results) == 0

    _after_test()

# ====================================================================================================
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ----------------------------------------------------------------------------------------------------
def test_create_and_get_tag():
    _before_test()

    name = "news"

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_tag(tag_name=name)
    
    results = chainlink.get_tag(1)
    assert len(results) == 1
    result_tag = results[0]
    assert isinstance(result_tag, api.Tag)
    assert result_tag.tag_id == 1
    assert result_tag.tag_name == name

    _after_test()

def test_update_tag_name():
    _before_test()

    first_name = "news"

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_tag(tag_name=first_name)
    
    results = chainlink.get_tag(1)
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == first_name

    second_name="__NEWS__"
    result_tag.update_tag_name(tag_name=second_name)

    results = chainlink.get_tag(1)
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == second_name

    _after_test()

def test_delete_tag():
    _before_test()

    name = "news"

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_tag(tag_name=name)
    
    results = chainlink.get_tag(1)
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == name

    chainlink.delete_tag(1)
    results = chainlink.get_tag(1)
    assert len(results) == 0

    _after_test()


def test_tag_search():
    _before_test()

    name = "news"

    chainlink = api.Chainlink.Builder().set_database_file_path(DB_PATH).build()
    chainlink.create_tag(tag_name=name)

    results = chainlink.get_tag(1)
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == name

    results = chainlink.search_tags_for_text('n')
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == name

    results = chainlink.search_tags_for_text('ne')
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == name

    results = chainlink.search_tags_for_text('ew')
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == name

    results = chainlink.search_tags_for_text('ws')
    assert len(results) == 1
    result_tag = results[0]
    assert result_tag.tag_name == name

    results = chainlink.search_tags_for_text('x')
    assert len(results) == 0

    _after_test()