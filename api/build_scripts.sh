#!/usr/bin/env bash

function main() {
    for cmd in "${@}"; do 
        case "${cmd}" in
            "clean") 
                rm -rf .pytest_cache __pycache__ reports build dist *.egg-info
                ;;
            "update")
                python -m pip install -r requirements.txt
                ;;
            "test")
                pytest -v -p no:cacheprovider --junitxml=reports/test_report.xml
                ;;
            "build") 
                python setup.py bdist_wheel 
                ;;
            *);;
        esac
    done
}
main "${@}"
