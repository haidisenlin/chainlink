INSERT INTO bookmarks VALUES (?, "https://www.duckduckgo.com", "DuckDuckGo", "Privacy-Friendly Search Engine");
INSERT INTO bookmarks VALUES (?, "https://www.google.com", "Google Search", "You know the drill...");
