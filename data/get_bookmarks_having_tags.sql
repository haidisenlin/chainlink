SELECT * FROM bookmarks WHERE id IN (
    SELECT bookmark_id FROM bookmark_tags WHERE tag_id IN (
        SELECT id FROM tags WHERE tag_name IN ("privacy")
    )
)